INTRODUCTION
------------

This module integrates the 'Velocity.js' library
  - https://github.com/julianshapiro/velocity

'Velocity.js' is an "Accelerated JavaScript Animation Engine" library.

What is Velocity.js?
Velocity.js is a lightweight JavaScript animation library that can be
used to create smooth animations and transitions on the web. It aims to
provide a fast and easy-to-use API for creating animations that perform
well on all devices and browsers. Velocity.js is designed to be a drop-in
replacement for jQuery’s $.animate() method, providing an API that is
almost identical but with much better performance.



FEATURES
--------

'Velocity.js' Key Features:

  - High performance: 
    Velocity.js uses optimized code to provide fast and smooth animations.

  - Short syntax:
    The API is designed to be simple and easy to use, with a short syntax
    that is similar to jQuery’s.

  - Chaining: 
    Velocity.js supports chaining, so you can apply multiple animations to
    the same element.

  - Easing functions:
    Velocity.js includes a wide variety of easing functions that can be
    used to create custom animations.

  - SVG support:
    Velocity.js can be used to animate SVG elements, including SVG paths.

  - Cross-browser compatibility: 
    Velocity.js works with all modern browsers, including IE 9+.


'Velocity.js' Use-Cases:

Here are some of the most common use cases for Velocity.js:

  - Animated page transitions: 
    Velocity.js can be used to create smooth transitions between pages on
    a website.

  - UI animations:
    Velocity.js is well-suited for creating animations for user interfaces,
    such as hover effects, tooltips, and pop-ups.

  - Data visualizations:
    Velocity.js can be used to animate data visualizations, such as charts
    and graphs.




REQUIREMENTS
------------

No requirement (library already included).


INSTALLATION
------------

Quick and easy to use that load Velocity library and UI pack on your pages:

1. Download 'Velocity' module - https://www.drupal.org/project/velocity

2. Extract and place it in the root of contributed modules directory i.e.
   /modules/contrib/velocity or /modules/velocity

3. Now, enable 'Velocity' module.


If you want to have more control over how to load, change the version,
use or not use the UI pack on pages (to limit it on some pages),
you can also activate the "Velocity UI" sub-module too.


USAGE
-----

It’s very simple to use a library, Add your script in theme/module js file.


BASIC USAGE EXAMPLE
===================

$("body").velocity({ opacity: 0.5 });

or

$element.velocity({
    properties: { opacity: 1 },
    options: { duration: 500 }
});


ADVANCED USAGE EXAMPLE
======================

$element.velocity({
    borderBottomWidth: [ "2px", "spring" ], // Uses "spring"
    width: [ "100px", [ 250, 15 ] ], // Uses custom spring physics
    height: "100px" // Defaults to easeInSine, the call's default easing
}, {
    easing: "easeInSine" // Default easing
});

or

$element.velocity({
    opacity: 0,
    tween: 1000 // Optional
}, {
    progress: function(elements, complete, remaining, start, tweenValue) {
        console.log((complete * 100) + "%");
        console.log(remaining + "ms remaining!");
        console.log("The current tween value is " + tweenValue)
    }
});


For more usage check official document: 

  - http://velocityjs.org/


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
----
https://codepen.io/collection/DPaxVA
