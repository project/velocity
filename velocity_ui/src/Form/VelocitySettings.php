<?php

namespace Drupal\velocity_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Velocity settings.
 */
class VelocitySettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'velocity_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get current settings.
    $config = $this->config('velocity.settings');

    // Let module handle load Velocity.js library.
    $form['load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load Velocity library'),
      '#default_value' => $config->get('load'),
      '#description'   => $this->t("If enabled, this module will attempt to load the Velocity library for your site. To prevent loading twice, leave this option disabled if you're including the assets manually or through another module or theme."),
    ];

    // Let module handle load Velocity.js UI pack library.
    $form['pack'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Attach Velocity UI pack'),
      '#default_value' => $config->get('pack'),
      '#description'   => $this->t("Enable this option if you want to use the Velocity UI pack as well."),
    ];

    // Load method library from CDN or Locally.
    $form['version'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Use version'),
      '#options'       => [
        'v1' => $this->t('Version 1.5.2 (latest stable)'),
        'v2' => $this->t('Version 2.0.6 (latest beta)'),
      ],
      '#default_value' => $config->get('version'),
      '#description'   => $this->t('Select the version of the Velocity library you want to use.'),
    ];

    // Load method library from CDN or Locally.
    $form['method'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Attach method'),
      '#options'       => [
        'local' => $this->t('Local'),
        'cdn'   => $this->t('CDN'),
      ],
      '#default_value' => $config->get('method'),
      '#description'   => $this->t('These settings control how the Velocity library is loaded. You can choose to load from the CDN (External source) or from the local (Already included in Velocity module).'),
    ];

    // Production or minimized version.
    $form['minimized'] = [
      '#type'  => 'details',
      '#title' => $this->t('Development or Production version'),
      '#open'  => TRUE,
    ];
    $form['minimized']['minimized_options'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Choose minimized or non-minimized library.'),
      '#options'       => [
        0 => $this->t('Use non-minimized library (Development)'),
        1 => $this->t('Use minimized library (Production)'),
      ],
      '#default_value' => $config->get('minimized.options'),
      '#description'   => $this->t('These settings work with both local library and CDN methods.'),
    ];

    // Load Velocity.js library Per-path.
    $form['url'] = [
      '#type'  => 'details',
      '#title' => $this->t('Load on specific URLs'),
      '#open'  => FALSE,
    ];
    $form['url']['url_visibility'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Load Velocity.js (and UI pack) on specific pages'),
      '#options'       => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
      '#default_value' => $config->get('url.visibility'),
    ];
    $form['url']['url_pages'] = [
      '#type'          => 'textarea',
      '#title'         => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _velocity_ui_array_to_string($config->get('url.pages')),
      '#description'   => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front'          => '<front>',
      ]),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated Velocity.js settings.
    $this->config('velocity.settings')
      ->set('load', $values['load'])
      ->set('pack', $values['pack'])
      ->set('version', $values['version'])
      ->set('method', $values['method'])
      ->set('minimized.options', $values['minimized_options'])
      ->set('url.visibility', $values['url_visibility'])
      ->set('url.pages', _velocity_ui_string_to_array($values['url_pages']))
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'velocity.settings',
    ];
  }

}
