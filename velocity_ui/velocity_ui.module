<?php

/**
 * @file
 * Drupal`s integration with Velocity.js library.
 *
 * Velocity.js - Accelerated JavaScript animation engine.
 *
 * Github:  https://github.com/julianshapiro/velocity
 * Website: http://velocityjs.org
 * license: MIT licensed
 *
 * Copyright (C) 2014-2023 Julian Shapiro
 */

use Drupal\Core\Installer\InstallerKernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Implements hook_page_attachments().
 */
function velocity_ui_page_attachments(array &$attachments) {
  // Don't add the library during installation.
  if (InstallerKernel::installationAttempted()) {
    return;
  }

  // Load the Velocity.js configuration settings.
  $config = \Drupal::config('velocity.settings');

  // Attach Velocity.js to pages with chosen configuration.
  $ui_pack = $config->get('pack');
  $version = $config->get('version');
  $methods = velocity_check_installed() ? $config->get('method') : 'cdn';
  $library = ['.', '.min.'];
  $variant = $library[$config->get('minimized.options')];

  // Don't include Velocity.js library if the user has
  // opted out of loading it or excluded from specified paths.
  if (!$config->get('load') || !_velocity_ui_check_url()) {
    return TRUE;
  }

  // Check first library load method.
  if ($methods == 'cdn') {
    // Load Velocity library with chosen version and variant from CDN.
    $attachments['#attached']['library'][] = 'velocity/velocity-cdn-' . $version . $variant . 'js';

    // Include Velocity UI pack if enabled.
    if ($ui_pack) {
      $attachments['#attached']['library'][] = 'velocity/velocity.ui-cdn-' . $version . $variant . 'js';
    }
  }
  else {
    // Load Velocity library with chosen version and variant from local.
    $attachments['#attached']['library'][] = 'velocity/velocity-' . $version . $variant . 'js';

    // Include Velocity UI pack if enabled.
    if ($ui_pack) {
      $attachments['#attached']['library'][] = 'velocity/velocity.ui-' . $version . $variant . 'js';
    }
  }
}

/**
 * Check if Velocity.js should be active for the current URL.
 *
 * @param \Symfony\Component\HttpFoundation\Request $request
 *   The request to use if provided, otherwise \Drupal::request() will be used.
 * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
 *   The request stack.
 *
 * @return bool
 *   TRUE if Velocity.js should be active for the current page.
 */
function _velocity_ui_check_url(Request $request = NULL, RequestStack $request_stack = NULL) {
  if (!isset($request)) {
    $request = \Drupal::request();
  }

  // Assume there are no matches until one is found.
  $page_match = FALSE;

  // Make it possible deactivate letting with
  // parameter ?animate=no in the url.
  $query = $request->query;
  if ($query->get('velocity') !== NULL && $query->get('velocity') == 'no') {
    return $page_match;
  }

  // Convert path to lowercase. This allows comparison of the same path
  // with different case. Ex: /Page, /page, /PAGE.
  $config = \Drupal::config('velocity.settings');
  $pages  = mb_strtolower(_velocity_ui_array_to_string($config->get('url.pages')));
  if (!$pages) {
    return TRUE;
  }
  if (!isset($request_stack)) {
    $request_stack = \Drupal::requestStack();
  }
  $current_request = $request_stack->getCurrentRequest();
  // Compare the lowercase path alias (if any) and internal path.
  $path = \Drupal::service('path.current')->getPath($current_request);
  // Do not trim a trailing slash if that is the complete path.
  $path = $path === '/' ? $path : rtrim($path, '/');
  $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $path_alias = mb_strtolower(\Drupal::service('path_alias.manager')->getAliasByPath($path, $langcode));
  $page_match = \Drupal::service('path.matcher')->matchPath($path_alias, $pages);
  if ($path_alias != $path) {
    $page_match = $page_match || \Drupal::service('path.matcher')->matchPath($path, $pages);
  }
  $page_match = $config->get('url.visibility') == 0 ? !$page_match : $page_match;

  return $page_match;
}

/**
 * Converts a text with lines (\n) into an array of lines.
 *
 * @return array
 *   Array with as many items as non-empty lines in the text
 */
function _velocity_ui_string_to_array($text) {
  if (!is_string($text)) {
    return NULL;
  }
  $text = str_replace("\r\n", "\n", $text);
  return array_filter(explode("\n", $text), 'trim');
}

/**
 * Converts an array of lines into a text with lines (\n).
 *
 * @return string
 *   Text with lines
 */
function _velocity_ui_array_to_string($array) {
  if (!is_array($array)) {
    return NULL;
  }
  return implode("\r\n", $array);
}
